#!/bin/bash
echo "script will execute in 10 seconds"
sleep 5s || exit 0
echo "script will execute in 5 seconds"
sleep 2s 
echo "script wil execute in 3 seconds"
sleep 1s
echo "script will execute in 2 secconds"
sleep 1s
echo "script will execute in 1 seccond"
sleep 1s

echo "------------------------------------------------------------------------------"
echo "-- Decator Install is now running, please do not cancel during installation --"
echo "------------------------------------------------------------------------------"

#check if git repo in the home directory
cd ~/Decator-install || gitrepo=1
if [ $gitrepo -eq 1 ]
then 
    echo "err: cannot find cloned git repo in the home directory!" 
    echo "quitting... error code: 3"
    exit 3
else 
    gitrepo=0
fi

############################################################
# Defining functions                                                                              
############################################################

function brave_install {
    git clone https://aur.archlinux.org/brave-bin.git
    cd brave-bin || exit
    yes | makepkg -si
    cd .. || exit
    sudo rm -rf brave-bin
}

function lf_terinal_file_browser {
    git clone https://aur.archlinux.org/lf-bin.git
    cd lf-bin || exit
    makepkg -si --noconfirm
}

function snap_install {
    git clone https://aur.archlinux.org/snapd.git
    cd snapd || exit
    makepkg -si --noconfirm
    sudo systemctl enable --now snapd.socket
    sudo ln -s /var/lib/snapd/snap /snap
}

function install_snap_packages {
    sudo snap install snap-store
    sudo snap install okular
    sudo snap install motrix
    sudo snap install obs-studio

}

function yay_install {
    cd /opt || exit
    git clone https://aur.archlinux.org/yay.git || echo "the yay git repo could not be cloned!"
    sudo chown -R "$USER":users ./yay
    cd yay || exit 1
    yes | makepkg -si
    cd .. || exit 1
    sudo rm -rf yay || echo "couldn't remove '/opt/yay' directory."
}

function clean_up {
    yes | yay -Syua
    yes | yay -Yc
    yes | yay -Syu
    yes | yay -Scc
    yes | sudo pacman -Scc
}

 function download_extract_add_fonts {
    truetype=0
    cd ~/ || exit
    git clone https://github.com/El-Wumbus/fonts
    cd fonts || exit
    cd /usr/share/fonts/truetype || truetype=1
    cd ~/fonts || exit
    sudo 7z e ~/fonts/Fonts.7z
    rm -rf Fonts.7z
    cd ~/ || exit
    if [ "$truetype" -gt 0 ]
    then 
        sudo  mv  -v ~/fonts/* /usr/share/fonts/TTF
    else 
        sudo mv  -v ~/fonts/* /usr/share/fonts/truetype/
    fi
    rm -rf Fonts.7z
    cd ..
    rm -rf fonts
    cd ~/ || exit
    cd ~/Decator-deb || exit
 }

############################################################
# Installing Programs                                                                       
############################################################
git --version || yes | sudo pacman -S git

#defining the pakages to install
PKGS=(
'python'
'solaar'
'code' #vscode
'flatpak'
'spectacle'
'pamac'
'mpv'
'terminator'
'samba'
'gnome-disk-utility'
'audacity'
'pavucontrol'
'kdenlive'
'reflector'
'python-pip'
'ardour'
'gimp'
'thunderbird'
'obs-studio'
'handbrake'
'p7zip-full'
'gedit'
'wine'
'winetricks'
'discord'
'lutris'
)

#updating packages
echo "-------------------------------------------------"
echo "-- updating existing packages and repositories --"
echo "-------------------------------------------------"
yay -Syua || yes | sudo pacman -Syu

#intstalling yay
echo "--------------------------------"
echo "-- installing yay AUR helper --"
echo "-------------------------------"
yay -Syua || yay_install
cd ~/Decator-install || exit

yes | sudo pacman -S cmake freetype2 fontconfig pkg-config make libxcb libxkbcommon python

#snapd && related packages
snap_install
sudo snap install snap-store
install_snap_packages 

#installing packages
for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

lf_terinal_file_browser


#installing AUR packages
yay -Syua --noconfirm || yes | sudo pacman -Syu 
yay -S github-desktop-bin --noconfirm
brave_install
yay -S freedownloadmanager --noconfirm
yay -S plex-media-server --noconfirm
yay -S sejda-desktop --noconfirm


#start services
sudo systemctl enable plexmediaserver.service || echo "err: plexmediaserver.service not found"
sudo systemctl start plexmediaserver.service || echo "err: plexmediaserver.service not found"

#install fonts
echo "
-------------------------------------------
-- adding fonts to /usr/share/fonts/TTF/ --
-------------------------------------------
"
download_extract_add_fonts

clean_up
echo "
--------------------------------------------------------------------------
-- finished running script, PLEASE REBOOT using 'sudo systemctl reboot' --
--------------------------------------------------------------------------
"
echo "script finished"
echo "Exit status: "
echo $?
echo "please visit https://el-wumbus.github.io/"
exit