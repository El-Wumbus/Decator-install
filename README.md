# Decator-install
#### _Just a very customizeable program install script for arch &amp; related Operating systems (incuding Manjaro)_
***
Run the script by running the following command in your preferred terminal emulator. **Must Be in the Home Directory**:


```markdown
cd ~/ && git clone https://github.com/El-Wumbus/Decator-install && cd Decator-install && sudo chmod u+x install-programs.sh && ./install-programs.sh
```
